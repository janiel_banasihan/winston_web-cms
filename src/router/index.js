import Vue from "vue";
import Router from "vue-router";

// Containers
const TheContainer = () => import("@/containers/TheContainer");

// Views
const Dashboard = () => import("@/views/Dashboard");

const Analytics = () => import("@/views/Analytics");
const RaffleEntries = () => import("@/views/RaffleEntries");
const RaffleRecordings = () => import("@/views/RaffleRecordings");
const RegistrationList = () => import("@/views/RegistrationList");
const SceneAuthoringTool = () => import("@/views/SceneAuthoringTool");
const Login = () => import("@/views/Login");
const Register = () => import("@/views/Register");
Vue.use(Router);

export default new Router({
  mode: "history",
  linkActiveClass: "active",
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes(),
});

function configRoutes() {
  return [
    {
      path: "/",
      name: "Login",
      component: Login,
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
    },
    {
      path: "/",
      redirect: "/dashboard",
      name: "Home",
      component: TheContainer,
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
        },
        {
          path: "analytics",
          name: "Analytics",
          component: Analytics,
        },
        {
          path: "raffle-entries",
          name: "RaffleEntries",
          component: RaffleEntries,
        },
        {
          path: "raffle-recordings",
          name: "RaffleRecordings",
          component: RaffleRecordings,
        },
        {
          path: "registration-list",
          name: "RegistrationList",
          component: RegistrationList,
        },
        {
          path: "scene-authoring-tool",
          name: "SceneAuthoringTool",
          component: SceneAuthoringTool,
        },
      ],
    },
  ];
}
