export default [
  {
    _name: "CSidebarNav",
    _children: [
      {
        _name: "CSidebarNavItem",
        name: "Dashboard",
        to: "/dashboard",
        icon: "cil-speedometer",
        badge: {
          color: "primary",
          text: "NEW",
        },
      },
      {
        _name: "CSidebarNavItem",
        name: "Analytics",
        to: "/analytics",
        icon: "cil-grid",
      },
      {
        _name: "CSidebarNavItem",
        name: "Raffle Entries",
        to: "/raffle-entries",
        icon: "cil-cursor",
      },
      {
        _name: "CSidebarNavItem",
        name: "Raffle Recordings",
        to: "/raffle-recordings",
        icon: "cil-list",
      },
      {
        _name: "CSidebarNavItem",
        name: "Registration List",
        to: "/registration-list",
        icon: "cil-pencil",
      },
      {
        _name: "CSidebarNavItem",
        name: "Scene Authoring Tool",
        to: "/scene-authoring-tool",
        icon: "cil-location-pin",
      },
    ],
  },
];
